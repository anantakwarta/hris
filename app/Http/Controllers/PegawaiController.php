<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterPegawai;
use Session;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\DB;


class PegawaiController extends Controller
{
    public function index(){
        $pegawai = MasterPegawai::all();
        return view('pegawai.index', compact(['pegawai']));
    }

    public function formtambah(){
        return view('pegawai.tambah');
    }

    public function tambah(Request $request){
        $foto = $request->file('foto');
        $file_foto = $foto;
        $nama_foto = time()."_".$file_foto->getClientOriginalName();
        $dir_foto = 'foto_pegawai';
        $file_foto->move($dir_foto,$nama_foto);
        $foto_uploaded = $nama_foto;

        $scanKK = $request->file('scanKK');
        $file_scanKK = $scanKK;
        $nama_scanKK = time()."_".$file_scanKK->getClientOriginalName();
        $dir_scanKK = 'scanKK';
        $file_scanKK->move($dir_scanKK,$nama_scanKK);
        $scanKK_uploaded = $nama_scanKK;

        $scanNPWP = $request->file('scanNPWP');
        $file_scanNPWP = $scanNPWP;
        $nama_scanNPWP = time()."_".$file_scanNPWP->getClientOriginalName();
        $dir_scanNPWP = 'scanNPWP';
        $file_scanNPWP->move($dir_scanNPWP,$nama_scanNPWP);
        $scanNPWP_uploaded = $nama_scanNPWP;

        $pegawai = Masterpegawai::create([
            'nip' => $request->input('nip'),
            'nik' => $request->input('nik'),
            'ktp' => $request->input('ktp'),
            'gelarDepan' => $request->input('gelarDepan'),
            'nama' => $request->input('nama'),
            'gelarBelakang' => $request->input('gelarBelakang'),
            'tempatLahir' => $request->input('tempatLahir'),
            'tanggalLahir' => $request->input('tanggalLahir'),
            'jenisKelamin' => $request->input('jenisKelamin'),
            'agama' => $request->input('agama'),
            'alamatKTP' => $request->input('alamatKTP'),
            'provinsiKTP' => $request->input('provinsiKTP'),
            'kotaKTP' => $request->input('kotaKTP'),
            'kecamatanKTP' => $request->input('kecamatanKTP'),
            'kelurahanKTP' => $request->input('kelurahanKTP'),
            'kodeposKTP' => $request->input('kodeposKTP'),
            'alamatTinggal' => $request->input('alamatTinggal'),
            'kotaTinggal' => $request->input('kotaTinggal'),
            'provinsiTinggal' => $request->input('provinsiTinggal'),
            'kecamatanTinggal' => $request->input('kecamatanTinggal'),
            'kelurahanTinggal' => $request->input('kelurahanTinggal'),
            'kodeposTinggal' => $request->input('kodeposTinggal'),
            'phone' => $request->input('phone'),
            'mobile' => $request->input('mobile'),
            'statusPerkawinan' => $request->input('statusPerkawinan'),
            'statusWajibPajak' => $request->input('statusWajibPajak'),
            'kk' => $request->input('kk'),
            'npwp' => $request->input('npwp'),
            'bpjstk' => $request->input('bpjstk'),
            'bpjskes' => $request->input('bpjskes'),
            'golonganDarah' => $request->input('golonganDarah'),
            'nomorRekamMedik' => $request->input('nomorRekamMedik'),
            'nomorSIM' => $request->input('nomorSIM'),
            'nomorRekening' => $request->input('nomorRekening'),
            'aktaKematian' => $request->input('aktaKematian'), 
            'createdBy' =>  Auth::id(),
            
            'foto' => $foto_uploaded,
            'scanKK' => $scanKK_uploaded,
            'scanNPWP' => $scanNPWP_uploaded
        ]);
        
        Session::flash('sukses', 'Data pegawai berhasil disimpan!');

        if($pegawai){   
            return redirect('pegawai');
        }
    }
}
