@extends('layouts.app', ['activePage' => 'pegawai', 'titlePage' => __('Tambah Pegawai')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('pegawai.tambah') }}" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
            {{ csrf_field() }}

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Tambah Pegawai') }}</h4>
                <p class="card-category">{{ __('Isi data dengan benar!') }}</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('NIP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nip" id="input-nip" type="text" placeholder="NIP" required="true" aria-required="true"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('NIK') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nik" id="input-nik" type="text" placeholder="{{ __('NIK') }}" required="true" aria-required="true"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('KTP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="ktp" id="input-ktp" type="text" placeholder="{{ __('KTP') }}" required="true" aria-required="true"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Gelar Depan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="gelardepan" id="input-gelardepan" type="text" placeholder="{{ __('Gelar Depan') }}" required="true" aria-required="true"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nama') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nama" id="input-nama" type="text" placeholder="{{ __('Nama') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Foto') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="file" name="foto">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Gelar Belakang') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="gelarbelakang" id="input-gelarbelakang" type="text" placeholder="{{ __('Gelar Belakang') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nama Panggilan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="namapanggilan" id="input-namapanggilan" type="text" placeholder="{{ __('Nama Panggilan') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Tempat Lahir') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="tempatLahir" id="input-tempatLahir" type="text" placeholder="{{ __('Tempat Lahir') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Tanggal Lahir') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="date" id="input-tanggalLahir" name="tanggalLahir" min="1960-01-01" max="2018-12-31">
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Jenis Kelamin') }}</label>
                  <div class="col-sm-7">
                    <select class="form-control" name="jenisKelamin" id="jenisKelamin" required="required">
                        <option value="Laki-laki">Laki-laki</option>
						<option value="Perempuan">Perempuan</option>
					</select>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Agama') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="agama" id="input-agama" type="text" placeholder="{{ __('Agama') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Alamat KTP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="alamatKTP" id="input-alamatKTP" type="text" placeholder="{{ __('Alamat KTP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Provinsi KTP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="provinsiKTP" id="input-provinsiKTP" type="text" placeholder="{{ __('Provinsi KTP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kota KTP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kotaKTP" id="input-kotaKTP" type="text" placeholder="{{ __('Kota KTP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kecamatan KTP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kecamatanKTP" id="input-kecamatanKTP" type="text" placeholder="{{ __('Kecamatan KTP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kelurahan KTP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="KelurahanKTP" id="input-KelurahanKTP" type="text" placeholder="{{ __('Kelurahan KTP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kode Pos KTP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kodeposKTP" id="input-kodeposKTP" type="text" placeholder="{{ __('Kode Pos KTP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Alamat Tinggal') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="alamatTinggal" id="input-alamatTinggal" type="text" placeholder="{{ __('Alamat Tinggal') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Provinsi Tinggal') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="provinsiTinggal" id="input-provinsiTinggal" type="text" placeholder="{{ __('Provinsi Tinggal') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kota Tinggal') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kotaTinggal" id="input-kotaTinggal" type="text" placeholder="{{ __('Kota Tinggal') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kecamatan Tinggal') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kecamatanTinggal" id="input-kecamatanTinggal" type="text" placeholder="{{ __('Kecamatan Tinggal') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kelurahan Tinggal') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kelurahanTinggal" id="input-kelurahanTinggal" type="text" placeholder="{{ __('Kelurahan Tinggal') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Kode Pos Tinggal') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kodeposTinggal" id="input-kodeposTinggal" type="text" placeholder="{{ __('Kode Pos Tinggal') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="email" id="input-email" type="email" placeholder="{{ __('Email') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No. Telepon') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="phone" id="input-phone" type="text" placeholder="{{ __('No. Telepon') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No. HP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="mobile" id="input-mobile" type="text" placeholder="{{ __('No. HP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Status Perkawinan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="statusPerkawinan" id="input-statusPerkawinan" type="text" placeholder="{{ __('Status Perkawinan') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Status Wajib Pajak') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                        <select class="form-control" name="statusWajibPajak" id="statusWajibPajak" required="required">
                            <option value="TK/0">
                                TK/0
                            </option>
                            <option value="TK/1">
                                TK/1
                            </option>
                            <option value="TK/2">
                                TK/2
                            </option>
                            <option value="TK/3">
                                TK/3
                            </option>
                            <option value="K/0">
                                K/0
                            </option>
                            <option value="K/1">
                                K/1
                            </option>
                        </select>
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No. KK') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="kk" id="input-kk" type="text" placeholder="{{ __('No. KK') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Scan KK') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="file" name="scanKK" required="required">
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No. NPWP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="npwp" id="input-npwp" type="text" placeholder="{{ __('No. NPWP') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Scan NPWP') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="file" name="scanNPWP" required="required">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('BPJS Ketenagakerjaan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="bpjstk" id="input-bpjstk" type="text" placeholder="{{ __('BPJS Ketenagakerjaan') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('BPJS Kesehatan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="bpjskes" id="input-bpjskes" type="text" placeholder="{{ __('BPJS Kesehatan') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Golongan Darah') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                        <select class="form-control" name="golonganDarah" id="golonganDarah" required="required">
                            <option value="A">
                                A
                            </option>
                            <option value="B">
                                B
                            </option>
                            <option value="AB">
                                AB
                            </option>
                            <option value="o">
                                o
                            </option>
                        </select>
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No. Rekam Medik') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nomorRekamMedik" id="input-nomorRekamMedik" type="text" placeholder="{{ __('No Rekam Medik') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No. SIM') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nomorSIM" id="input-nomorSIM" type="text" placeholder="{{ __('No. SIM') }}"  />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('No. Rekening') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="nomorRekening" id="input-nomorRekening" type="text" placeholder="{{ __('No. Rekening') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Golongan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="golongan" id="input-golongan" type="text" placeholder="{{ __('Golongan') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Profesi') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="profesi" id="input-profesi" type="text" placeholder="{{ __('Profesi') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Jabatan') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="jabatan" id="input-jabatan" type="text" placeholder="{{ __('Jabatan') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('SOTK') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="sotk" id="input-sotk" type="text" placeholder="{{ __('SOTK') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('MKG') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="mkg" id="input-mkg" type="text" placeholder="{{ __('MKG') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Gaji Pokok') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="gapok" id="input-gapok" type="text" placeholder="{{ __('Gaji Pokok') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Unit') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="unit" id="input-unit" type="text" placeholder="{{ __('Unit') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Status Kepegawaian') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="statusKepegawaian" id="input-statusKepegawaian" type="text" placeholder="{{ __('Status Kepegawaian') }}" required="true" aria-required="true" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Akta Kematian') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="aktaKematian" id="input-aktaKematian" type="text" placeholder="{{ __('Akta Kematian') }}"  />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Tanggal Pensiun') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="tanggalPensiun" id="input-tanggalPensiun" type="text" placeholder="{{ __('Tanggal Pensiun') }}"  />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Tanggal Kematian') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="tanggalKematian" id="input-tanggalKematian" type="text" placeholder="{{ __('Tanggal Kematian') }}" />
                    </div>
                  </div>
                </div><div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Jenjang Karir') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="jenjangKarirId" id="input-jenjangKarirId" type="text" placeholder="{{ __('Jenjang Karir') }}"  />
                    </div>
                  </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection