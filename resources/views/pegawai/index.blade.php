@extends('layouts.app', ['activePage' => 'pegawai', 'titlePage' => __('Pegawai')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Data Pegawai</h4>
            <p class="card-category"><a class="nav-link" href="{{ route('pegawai.formtambah') }}">+ Tambah Data Pegawai</p></a>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    Nama
                  </th>
                  <th>
                    NIP
                  </th>
                  <th>
                    Profesi
                  </th>
                  <th>
                    Jabatan
                  </th>
                  <th>
                      Aksi
                  </th>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection